playlist = {
    "name": "Cassidy's Favorites",
    "author": "cassidy",
    "songs": [
        {
            "name": "Yesterday",
            "artist": "The Beatles",
            "album": "Help!",
            "duration": 2.05
        },
        {
            "name": "Head Like a Hole",
            "artist": "Nine Inch Nails",
            "album": "Pretty Hate Machine",
            "duration": 5.01
        },
        {
            "name": "Immigrant Song" ,
            "artist": "Led Zeppelin",
            "album": "Led Zeppelin III",
            "duration": 2.43
        },
        {
            "name": "Every Breath You Take",
            "artist": "The Police",
            "album": "Synchronicity",
            "duration": 4.21
        }
    ]
}

total_length = 0
for song in playlist["songs"]:
    total_length += song["duration"]
print(total_length)
