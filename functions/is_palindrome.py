'''
is_palindrome('testing') # False
is_palindrome('tacocat') # True
is_palindrome('hannah') # True
is_palindrome('robert') # False
is_palindrome('amanaplanacanalpanama') # True
'''

def is_palindrome(string):
    stripped = string.replace(" ", "").lower()
    return stripped == stripped[::-1]
print(is_palindrome("Hannah"))
