def return_day(number):
    days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]
    if number > 0 and number <= len(days):
        return days[number - 1]
    return None

print(return_day(8))
