def single_letter_count(string,letter):
    letter_count = list(string.lower()).count(letter)
    if letter_count > 0:
        return letter_count
    return 0

print(single_letter_count("Hello", "h"))
