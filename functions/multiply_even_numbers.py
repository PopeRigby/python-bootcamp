def multiply_even_numbers(list_of_nums):
    total = 1
    for number in list_of_nums:
        if number % 2 == 0:
            total = total * number
    return total
print(multiply_even_numbers([2,3,4,5,6]))
