import random

min_number = 1
max_number = 10

computer_number = random.randint(min_number,max_number) # generate computer's number

game_won = False
wants_to_play = True

print("Welcome to the Guessing Game!")
# game logic
while wants_to_play == True:
	while game_won == False:
		user_guess = input(f"Guess a number between {min_number} and {max_number}: ")
		user_guess = int(user_guess)
		# if they guess correct, tell them they won
		if user_guess == computer_number:
			print("Good job! You guessed it")
			break
		# otherwise, tell them they are too high or too low
		elif user_guess < computer_number:
			print("Too low, guess again!")
		elif user_guess > computer_number:
			print("Too high, guess again!")
		else:
			print("Invalid input")
			break
	# let the player play again if they want
	play_again = input("Want to play again? (y/n) ")
	if play_again.lower() == "y" or play_again == "yes":
		wants_to_play = True
	elif play_again.lower() == "n" or play_again == "no":
		print("Thank you for playing!")
		wants_to_play = False
	else:
		print("Invalid input")
		break