# Append
# "Appends" the arg at the end of the list (only takes one item)
test_list_1 = [1,2,3,4]
test_list_1.append(5)
print(test_list_1) # 1,2,3,4,5

# Insert
# "Inserts" an item at a given index
test_list_2 = [6,7,9,10]
test_list_2.insert(2, 8)
print(test_list_2) # 1,2,3,4,5

# Extend
# Same as append, but can take multiple items in a list
test_list_3 = [11,12]
test_list_3.extend([13,14,15])
print(test_list_3) # 1,2,3,4,5