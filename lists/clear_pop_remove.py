# Clear
# Clears the entire list ¯\_(ツ)_/¯
test_list_1 = [1,2,3,4,5]
test_list_1.clear()
print(test_list_1) # []

# Pop
# Removes an item at the specified index, and then returns that item
# Removes the last item if no index is specified
test_list_2 = [6,7,8,9,10]
test_list_2.pop(2)
print(test_list_2) # 6,7,9,10

test_list_2.pop()
print(test_list_2) # 6,7,9

# Remove
# Removes the first item from the list whose value is x
# If there are multiple items w/ the same value, only the first one found is removed
# Throws a ValueError, if the item is not found
test_list_3 = [1,2,3,4,4,4]
test_list_3.remove(2)
print(test_list_3) # 1,3,4,4,4

test_list_3.remove(4)
print(test_list_3) # 1,3,4,4
